from setuptools import setup

install_requires = [
    'Django>=1.9',
    'vobject>=0.9.5',
]

setup(
    install_requires=install_requires,
)
