Django Staff Contacts
^^^^^^^^^^^^^^^^^^^^^

This Django package provides contacts for staff users. These contacts have
no public views and are meant to share information between site staff only.
While it is certainly possible to create public views for these contacts,
it is not and never will be the focus of this project.

This is by no means a full-fledged CRM application. It was born during the
development of a site, with several people in different locations and
having access to all contacts and information about those contacts
improved collaboration.

Quickstart
^^^^^^^^^^

Install using pip:

    % pip install django_staff_contacts

Update your project's settings:

    INSTALLED_APPS = [
        # ...
        "staff_contacts.apps.StaffContactsConfig",
    ]

Run migrations:

    % python manage.py migrate

You can now login to the admin and start adding contacts allthough it is
recommended to start adding properties first.
